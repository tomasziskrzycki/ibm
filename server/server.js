const Promise = require('bluebird');
const express = require('express');
const bodyParser = require('body-parser')
const db = require('/QOpenSys/QIBM/ProdData/OPS/Node6/os400/db2i/lib/db2a');

const app = express();
const port = process.env.PORT || 5000;

const DBname = '*LOCAL';
const userId = 'STD_07';
const passwd = 'abc123';
const tableName = 'QGPL.PROJECT';
const errStr = 'SQLCODE=';
const noTableErrCode = -204

app.use(bodyParser.json());
app.use(express.json());

queryDb = (sqlCommand) => {
  return new Promise((resolve, reject) => {
    console.log(sqlCommand);
    const dbconn = new db.dbconn();
    dbconn.conn(DBname, userId, passwd);
    const stmt = new db.dbstmt(dbconn);
    stmt.exec(sqlCommand, (result, err) => {
      stmt.close();
      dbconn.disconn();
      dbconn.close();
      if (err) {
        console.log(err);
        errorCode = 1.0 * err.split('SQLCODE=')[1].split(' ')[0];
        if ( errorCode < 0 ) {
          reject(errorCode);
        }
      }
      resolve(result);
    });
  });
}

app.get('/api/select', (req, res) => {
  const sql = `SELECT * FROM ${tableName}`;
  queryDb(sql).then((result) => {
    res.send(result);
  }).catch(err => {
    if( err === noTableErrCode ) {
      const sql = `CREATE TABLE ${tableName}(` +
      `ID integer not null GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),` +
      `NAME varchar(30), SURNAME varchar(30),EMAIL varchar(50), TEL_NUMBER char(9))`;
      queryDb(sql).then((result) => {
        res.send(result);
      })
    }
  })
});

app.post('/api/insert', (req, res) => {
  const sql =
    `INSERT INTO ${tableName} (NAME, SURNAME, EMAIL, TEL_NUMBER) VALUES(` +
    `'${req.body.NAME}','${req.body.SURNAME}',` +
    `'${req.body.EMAIL}','${req.body.TEL_NUMBER}') WITH NC`;
  queryDb(sql).then((result) => {
    res.send(result);
  })
});

app.post('/api/update', (req, res) => {
  const sql =
    `UPDATE ${tableName} SET NAME='${req.body.NAME}', ` +
    `SURNAME='${req.body.SURNAME}', EMAIL='${req.body.EMAIL}', ` +
    `TEL_NUMBER='${req.body.TEL_NUMBER}' WHERE ID=${req.body.ID} WITH NC`;
  queryDb(sql).then((result) => {
    res.send(result);
  })
});

app.post('/api/delete', (req, res) => {
  const sql = `DELETE FROM ${tableName} WHERE ID=${req.body.ID} WITH NC`;
  queryDb(sql).then((result) => {
    res.send(result);
  })
});

app.listen(port, () => console.log(`API listening on port ${port}`));