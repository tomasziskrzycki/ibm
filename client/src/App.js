import React, { Component } from 'react';
import './App.css';
import { AppHeader } from './AppHeader.js'
import { DbList } from './DbList.js';
import { Form } from './Form.js'

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      view: 'table',
      operation: null
    };
    this.data = null;
  }

  addClick = () => {
    this.setState({view: 'form', operation: 'add'});
  }

  editClick = (row) => {
    this.data = row;
    this.setState({view: 'form', operation: 'edit'});
  }

  deleteClick = (id) => {
    this.callDelete(id)
      .then(this.dbList.callApi())
      .catch(this.dbList.callApi());
  }

  callDelete = async (id) => {
    const proxyUrl = 'https://cors-anywhere.herokuapp.com/';
    const response = await fetch(proxyUrl+'http://156.17.43.148:5000/api/delete', {
        method: 'POST',
        body: JSON.stringify({'ID': id}),
        headers: {
            'Content-Type': 'application/json'
        }
      });
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  submitClick = (data) => {
    this.data = null;
    const proxyUrl = 'https://cors-anywhere.herokuapp.com/';
    if(this.state.operation === 'add') {
      fetch(proxyUrl+'http://156.17.43.148:5000/api/insert', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }})
      .then( () => {
        this.setState({view: 'table'});
        this.dbList.callApi();
      })
      .catch( () => {
        this.setState({view: 'table'});
        this.dbList.callApi();
      })
    }
    if(this.state.operation === 'edit') {
      fetch(proxyUrl+'http://156.17.43.148:5000/api/update', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }})
      .then( () => {
        this.setState({view: 'table'});
        this.dbList.callApi();
      })
      .catch( () => {
        this.setState({view: 'table'});
        this.dbList.callApi();
      })
    }
  }

  renderAppBody = () => {
    if (this.state.view === 'table') {
      return (
        <DbList ref={instance => { this.dbList = instance; }} editClick={this.editClick} deleteClick={this.deleteClick} />
      )
    }
    if (this.state.view === 'form') {
      return (
        < Form submitClick={this.submitClick} data={this.data}/>
      )
    }
  }

  render() {
    return (
      <div className='App'>
        <AppHeader addClick={this.addClick} view={this.state.view}/>
        <div className='AppBody'>
          {this.renderAppBody()}
        </div>
      </div>
    );
  }
}
