import React, { Component } from 'react';
import './App.css';

export class AppHeader extends Component {

  renderAddButton = () => {
    if (this.props.view === 'table')
      return (
        <div className='AddButton' onClick={() => { this.props.addClick() }} />
      )
    return (null)
  }

  render() {
    return (
      <div className='AppHeader'>
        <div className='AppTitle'>
          IBM DB
        </div>
        {this.renderAddButton()}
      </div>
    );
  }
}