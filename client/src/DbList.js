import React, { Component } from 'react';
import './DbList.css';

export class DbList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: null
    };
  }

  componentDidMount() {
    this.callApi()
  }

  callApi = () => {
    this.callSelect()
      .then(res => this.setState({ list: res }))
      .catch(err => console.log(err));
  }

  callSelect = async () => {
    const proxyUrl = 'https://cors-anywhere.herokuapp.com/';
    const response = await fetch(proxyUrl+'http://156.17.43.148:5000/api/select');
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  renderList = () => {
    if (!this.state.list)
      return (null);
    return this.state.list.map((item) => {
      return (
        <tr>
          <td>{item.ID + ' '}</td>
          <td>{item.NAME + ' '}</td>
          <td>{item.SURNAME + ' '}</td>
          <td>{item.EMAIL + ' '}</td>
          <td>{item.TEL_NUMBER}</td>
          <td><div className='EditButton'
            onClick={() => { this.props.editClick(item) }} /></td>
          <td><div className='DeleteButton'
            onClick={() => { this.props.deleteClick(item.ID) }} /></td>
        </tr>
      );
    });
  }

  render() {
    if (!this.state.list)
      return (null);
    return (
      <div className='ListItem'>
        <table>
          <tbody>
            <tr>
              <th>ID</th>
              <th>Firstname</th>
              <th>Lastname</th>
              <th>Email</th>
              <th>Tel number</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
            {this.renderList()}
          </tbody>
        </table>
      </div>
    );
  }
}