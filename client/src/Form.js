import React, { Component } from 'react';
import './Form.css';

export class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        NAME: '',
        SURNAME: '',
        EMAIL: '',
        TEL_NUMBER: '',
        ID: ''
      }
    }
    if(props.data !== null){
      this.state.data= props.data;
    }
  }

  handleChange = (event) => {
    let data = { ...this.state.data }
    data[event.target.name] = event.target.value;
    this.setState({ data });
  }

  handleSubmit = (event) => {
    this.props.submitClick(this.state.data);
    event.preventDefault();
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.data !== null){
      this.setState({data: nextProps.data});
    }
  }

  render() {
    return (
      <div className='form' >
        <form >
          <p>
            <label>
              Name:
          </label>
            <input name='NAME' type='text' value={this.state.data.NAME} onChange={this.handleChange} />
          </p>
          <p >
            <label>
              Surname:
          </label>
            <input name='SURNAME' type='text' value={this.state.data.SURNAME} onChange={this.handleChange} />
          </p>
          <p >
            <label>
              E-mail:
          </label>
            <input name='EMAIL' type='text' value={this.state.data.EMAIL} onChange={this.handleChange} />
          </p>
          <p >
            <label>
              Tel. number:
          </label>
            <input name='TEL_NUMBER' type='text' value={this.state.data.TEL_NUMBER} onChange={this.handleChange} />
          </p>
        </form>
        <br />
        <button onClick={this.handleSubmit} className='SubmitButton' >Submit</button>
      </div>
    );
  }
}